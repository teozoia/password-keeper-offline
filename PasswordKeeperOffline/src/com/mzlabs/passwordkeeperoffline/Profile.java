package com.mzlabs.passwordkeeperoffline;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.MessageDigest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.view.View;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;

public class Profile extends Activity {
	
	Button btNext;
	String path="profile.json";
	JSONObject profile=new JSONObject();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		
		btNext=(Button)findViewById(R.id.acProBtNext);
		
		btNext.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
            	
            	String first=(((EditText)findViewById(R.id.acProFirstName)).getText()).toString();
            	String last=(((EditText)findViewById(R.id.acProLastName)).getText()).toString();
            	String pass1=(((EditText)findViewById(R.id.acProPasscode1)).getText()).toString();
            	String pass2=(((EditText)findViewById(R.id.acProPasscode2)).getText()).toString();
            	
            	if(first.equalsIgnoreCase("")||
        			last.equalsIgnoreCase("")||
        			pass1.equalsIgnoreCase("")||
        			pass2.equalsIgnoreCase("")){
            		//almeno un campo � vuoto
            		Toast.makeText(getApplicationContext(),"Fill all field to continue!", Toast.LENGTH_LONG).show();
            	}else{
            		if(pass1.equals(pass2)){
            			//tutto ok
            			//cancello uno dei due passcode
            			pass1=null;
            			pass2=criptaPass(pass2);
            			profile=toJsonProfile(first,last,pass2);
            			writeProfile(path,profile);
            		}else{
            			//le password non corrispondono
            			Toast.makeText(getApplicationContext(),"Passcode don't match!", Toast.LENGTH_LONG).show();
            		}
            	}
            }
        });
	}
	//---------------------------------------------------------------------------------------
	public String criptaPass(String pass){
		
		try {
            String plaintext = pass;
            MessageDigest m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plaintext.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1,digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while(hashtext.length() < 32 ){
                hashtext = "0"+hashtext;
            }
            pass=hashtext;
        }catch(Exception e){
            e.printStackTrace();
        }
		
		return pass;
	}
	//-----------------------------------------------------------------------------------------
	public JSONObject toJsonProfile(String first,String last,String pass){
		
		JSONObject p=new JSONObject();
			
		try {
            p.put("first", first);
            p.put("last", last);
            p.put("passcode", pass);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
			
		return p;
	}
	//------------------------------------------------------------------------------------------
	public void writeProfile(String path,JSONObject p){
		
        String message=p.toString();
        try{
            FileOutputStream fos=openFileOutput(path, MODE_PRIVATE);
            OutputStreamWriter osw= new OutputStreamWriter(fos);
            try{
                osw.write(message);
                osw.flush();
                osw.close();
                Toast.makeText(getApplicationContext(), "Profile successfully created!", Toast.LENGTH_LONG).show();
                Intent a_Main = new Intent(Profile.this, MainActivity.class);
                finish();
                Profile.this.startActivity(a_Main);
            }catch(IOException e){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
        
	}
	//------------------------------------------------------------------------------------------
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.profile, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
