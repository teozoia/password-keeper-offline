package com.mzlabs.passwordkeeperoffline;

import android.app.Activity;
import android.os.Bundle;
import android.util.LogPrinter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.ActionBarDrawerToggle;
//import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.widget.ArrayAdapter;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.RelativeLayout;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity implements OnItemClickListener{
	
	private DrawerLayout drawerLayout;
	private ListView listView;
	private String[] menu;
	private ActionBarDrawerToggle drawerListener;
	private TextView nomeProfilo, numAccount;
	
	public String path="profile.json";
	public String pathAccount="account19.json";
	public int posx;
	
	ListView lv;
    List<ListViewItem> items;
    CustomListViewAdapter adapter;
    final Context context = this;
    
    CustomListViewAdapter adapter2;
    
    ListView lv3;
    List<ListViewItem> items3;
    CustomListViewAdapter adapter3;
    
    RelativeLayout item;
    
    
    int i;
    int flag;
    int ac=0;
    boolean flag2=false;
    
    ArrayList <String> nome=new ArrayList <String>();
	ArrayList <String> user=new ArrayList <String>();
	ArrayList <String> psw=new ArrayList <String>();
	
	//fragment add
	Button fragAddBtNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        //verifico se esiste un profilo
        verificaProfilo(readJson(path));
        
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
        menu=getResources().getStringArray(R.array.menu);
        listView=(ListView)findViewById(R.id.drawerList);
        listView.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu){
        	
        	@Override
            public View getView(int position, View convertView,
                    ViewGroup parent) {
                View view =super.getView(position, convertView, parent);

                TextView textView=(TextView) view.findViewById(android.R.id.text1);

                textView.setTextColor(Color.parseColor("#CCCCCC"));

                return view;
            }
        });
        listView.setOnItemClickListener(this);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
        drawerListener=new ActionBarDrawerToggle(this,drawerLayout,R.drawable.ic_drawer,
        		R.string.drawer_open,R.string.drawer_close){
        	@Override
        	public void onDrawerClosed(View drawerView){
        		//Toast.makeText(MainActivity.this, "Drawer chiuso", Toast.LENGTH_SHORT).show();
        	}
        	@Override
        	public void onDrawerOpened(View drawerView){
        		//Toast.makeText(MainActivity.this, "Drawer aperto", Toast.LENGTH_SHORT).show();
        	}
        };
        drawerLayout.setDrawerListener(drawerListener);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig){
    	super.onConfigurationChanged(newConfig);
    	drawerListener.onConfigurationChanged(newConfig);
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item){
    	if(drawerListener.onOptionsItemSelected(item)){
    		return true;
    	}
    	return super.onOptionsItemSelected(item);
    }
    
    @Override
    protected void onPostCreate(Bundle savedInstanceState){
    	super.onPostCreate(savedInstanceState);
    	drawerListener.syncState();
    }
    
    
    @Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		//Toast.makeText(this, "Elemento: "+menu[position], Toast.LENGTH_SHORT).show();
		selectItem(position);
		drawerLayout.closeDrawer(Gravity.START);
		
		item = (RelativeLayout)findViewById(R.id.main);
		item.removeAllViews();
		item.addView(getLayoutInflater().inflate(R.layout.account_frag, null));
		
		switch(position){
		case 0:
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
			nomeProfilo=(TextView)findViewById(R.id.profilo);
			numAccount=(TextView)findViewById(R.id.nAccount);
			try {
				nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
				numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
					numAccount.setText("1");	
				}else{
					numAccount.setText("0");
				}
			}
			Button setup=(Button)findViewById(R.id.setup);
	        setup.setOnClickListener(new View.OnClickListener(){
	            public void onClick(View view){
	            	
	            	Toast.makeText(getApplicationContext(),"Not available now\n(Please wait v. 0.5.3)", Toast.LENGTH_LONG).show();
                    	            	
	            }
	        });
			
			break;
		case 1:
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.passcode_frag, null));
			
			flag=0;//non ver
			//try {
				Button btNext=(Button)findViewById(R.id.btNext);
		        btNext.setOnClickListener(new View.OnClickListener(){
		            public void onClick(View view){
		            	if(verificaPasscode(criptaPass(((EditText)findViewById(R.id.passcode)).getText().toString()))){
		            		flag=1;
		            		item = (RelativeLayout)findViewById(R.id.main);
		            		item.removeAllViews();
							item.addView(getLayoutInflater().inflate(R.layout.account_frag, null));
							try {
								outputListView(new JSONObject (readJson(pathAccount)));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}else{
							TextView er=(TextView)findViewById(R.id.error);
							er.setVisibility(View.VISIBLE);
						}
		            }
				});
			break;
		case 2:
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.add_frag, null));
			
			//load button
			fragAddBtNext=(Button)findViewById(R.id.frag_add_bt_next);
			
			//---------------frag add------------------
	        fragAddBtNext.setOnClickListener(new View.OnClickListener(){
	            public void onClick(View view){
	            	String fragAddNomeAcc=(((EditText)findViewById(R.id.frag_add_ac_name)).getText()).toString();
	            	String fragAddUser=(((EditText)findViewById(R.id.frag_add_ac_user)).getText()).toString();
	            	String fragAddPsw=(((EditText)findViewById(R.id.frag_add_ac_pass)).getText()).toString();
	            	
	            	//fragAddPsw=criptaPass(fragAddPsw);
	            	writeJsonAccount(readJson(pathAccount),fragAddNomeAcc,fragAddUser,fragAddPsw);
	            	item.removeAllViews();
					item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
					nomeProfilo=(TextView)findViewById(R.id.profilo);
					numAccount=(TextView)findViewById(R.id.nAccount);
					try {
						nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
						numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
					} catch (JSONException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
							numAccount.setText("1");	
						}else{
							numAccount.setText("0");
						}
					}
	            }
	            
			});
	        
			break;
		case 3:
			
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.settings_frag, null));
			
			ListView lv3 = (ListView) findViewById(R.id.listSetting);
	        ArrayList <ListViewItem> items3  = new ArrayList<ListViewItem>();
	        
	        items3.add(new ListViewItem()
            {{
                    Title = "Bug or problem";
                    SubTitle = "Send a message";
            }});
	        
	        items3.add(new ListViewItem()
            {{
                    Title = "About developer";
                    SubTitle = "Matteo Zoia";
            }});
        	
        	
        	try {
				items3.add(new ListViewItem()
				{{
				        Title = "App version";
				        SubTitle = (getPackageManager().getPackageInfo(getPackageName(), 0)).versionName+" (beta)";
				}});
			} catch (NameNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	        
	        
	        CustomListViewAdapter adapter3 = new CustomListViewAdapter(this, items3);
	        lv3.setAdapter(adapter3);
	        
	        lv3.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> adattatore, final View componente, int pos, long id){
	         	    
	            	switch(pos){
	            		case 0:
	            			String exit="";
	            			Intent i = new Intent(Intent.ACTION_SEND);
	            			i.setType("message/rfc822");
	            			i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"zoiateo@gmail.com"});
	            			i.putExtra(Intent.EXTRA_SUBJECT, "BUG-pswKep");
	            			try {
	            				  Process process = Runtime.getRuntime().exec("logcat");
	            				  BufferedReader bufferedReader = new BufferedReader(
	            				  new InputStreamReader(process.getInputStream()));

	            				  StringBuilder log=new StringBuilder();
	            				  String line = "";
	            				  log.append(line);
	            				  exit=log.toString();
	            				  } 
	            				catch (IOException e) {}
	            			i.putExtra(Intent.EXTRA_TEXT   , exit);
	            			try {
	            			    startActivity(Intent.createChooser(i, "Send mail..."));
	            			} catch (android.content.ActivityNotFoundException ex) {
	            			    Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
	            			}
	            			
	            			break;
	            		case 1:
	            			//stampa info su matteo zoia
	            			String mesDialog="Hey, first of all THANK YOU for downloading the app. \n\n" +
	            					"I'm Matteo Zoia, 19 years old Italy, Mesero (MI).\n\n" +
	            					"Here's something interesting about me:" +
	            					"twitter.com/teozoia.";

	                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
	                        // set title
	                        alertDialogBuilder.setTitle("About developer");
	                        // set dialog message
	                        alertDialogBuilder
	                                .setMessage(mesDialog)
	                                .setCancelable(false)
	                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	                                    public void onClick(DialogInterface dialog, int id) {
	                                        dialog.cancel();
	                                    }
	                                });
	                        // create alert dialog
	                        AlertDialog alertDialog = alertDialogBuilder.create();
	                        // show it
	                        alertDialog.show();
	            			
	            			break;
	            	}
	                
	            }
	        });
			
			break;
		case 4:
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.file_frag, null));
			
			ListView lv2 = (ListView) findViewById(R.id.listFile);
	        ArrayList <ListViewItem> items2  = new ArrayList<ListViewItem>();
	        
        	items2.add(new ListViewItem()
            {{
                    Title = "Info Profile";
                    SubTitle = path;
            }});
        	
        	items2.add(new ListViewItem()
            {{
                    Title = "Account store";
                    SubTitle = pathAccount;
            }});
	        
	        
	        CustomListViewAdapter adapter2 = new CustomListViewAdapter(this, items2);
	        lv2.setAdapter(adapter2);
	        
	        lv2.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> adattatore, final View componente, int pos, long id){
	         	    
	            	Intent a_ReadFile = new Intent(MainActivity.this, ReadFile.class);
	            	
	            	if(pos==0){
	            		//info profilo
	            		a_ReadFile.putExtra("title", "Info Profile");
	            		a_ReadFile.putExtra("path", path);
	            		a_ReadFile.putExtra("json", readJson(path));
	            		
	            	}else{
	            		//info account
	            		nome.clear();
	                	user.clear();
	                	psw.clear();
	                	
	                	String jsonPass="";
	            		
	            		try {
	            			JSONObject json=new JSONObject(readJson(pathAccount));
	            			
	        				JSONArray array=new JSONArray (json.getString("nome"));
	        				for(int i=0;i<array.length();i++){
	        					nome.add(array.getString(i));
	        				}
	        				array=new JSONArray (json.getString("user"));
	        				for(int i=0;i<array.length();i++){
	        					user.add(array.getString(i));
	        				}
	        				array=new JSONArray (json.getString("psw"));
	        				for(int i=0;i<array.length();i++){
	        					psw.add(array.getString(i));
	        				}
	        				
	        				jsonPass+="{\n";
	        				for(int i=0;i<array.length();i++){
	        					jsonPass+="\t[\n";
	        					jsonPass+="\t\tname:"+nome.get(i)+",\n";
	        					jsonPass+="\t\tuser:"+user.get(i)+",\n";
	        					jsonPass+="\t\tpsw:"+criptaPass(psw.get(i))+",\n";
	        					jsonPass+="\t],\n";
	        				}
	        				jsonPass+="}";
	            		
	        		} catch (JSONException e) {
	        		// TODO Auto-generated catch block
	        		e.printStackTrace();
	        		}
	            		
	            		a_ReadFile.putExtra("title", "Info Account");
	            		a_ReadFile.putExtra("path", pathAccount);
	            		a_ReadFile.putExtra("json", jsonPass);
	            	}
	            	
	                MainActivity.this.startActivity(a_ReadFile);
	                
	            }
	        });
	        
			break;
		case 5:
			View child = getLayoutInflater().inflate(R.layout.credit_frag, null);
			item.removeAllViews();
			item.addView(child);
			break;
		default:
			item.removeAllViews();
			item.addView(getLayoutInflater().inflate(R.layout.error_frag, null));
			break;
			
		}
	}
    //----------------------------------------------------------------------------
    void outputListView(JSONObject json){
    	
    	nome.clear();
    	user.clear();
    	psw.clear();
    	
    	try {
				JSONArray array=new JSONArray (json.getString("nome"));
				for(int i=0;i<array.length();i++){
					nome.add(array.getString(i));
				}
				array=new JSONArray (json.getString("user"));
				for(int i=0;i<array.length();i++){
					user.add(array.getString(i));
				}
				array=new JSONArray (json.getString("psw"));
				for(int i=0;i<array.length();i++){
					psw.add(array.getString(i));
				}
    		
		} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		try {
			nome.add(json.getString("nome"));
			user.add(json.getString("user"));
			psw.add(json.getString("psw"));
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
			
		}
    	
    	
    	ListView lv = (ListView) findViewById(R.id.list);
        ArrayList <ListViewItem> items  = new ArrayList<ListViewItem>();
        
        for(i=0;i<nome.size();i++){
        	items.add(new ListViewItem()
            {{
                    Title = nome.get(i);
                    SubTitle = user.get(i);
            }});
        }
        
        adapter = new CustomListViewAdapter(this, items);
        lv.setAdapter(adapter);
        
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adattatore, final View componente, int pos, long id){

                String nomeAc=nome.get(pos);
                String users=user.get(pos);
                String pas=psw.get(pos);
                
                posx=pos;


                String mesDialog="Username: "+users+"\n"+
                        "Password: "+(pas);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set title
                alertDialogBuilder.setTitle(nomeAc);
                // set dialog message
                alertDialogBuilder
                        .setMessage(mesDialog)
                        .setCancelable(false)
                    	.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                
                                deleteItem(posx);
                                item.removeAllViews();
    							item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
    							nomeProfilo=(TextView)findViewById(R.id.profilo);
    							numAccount=(TextView)findViewById(R.id.nAccount);
    							try {
    								nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
    								numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
    							} catch (JSONException e1) {
    								// TODO Auto-generated catch block
    								e1.printStackTrace();
    								if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
    									numAccount.setText("1");	
    								}else{
    									numAccount.setText("0");
    								}
    							}
    							dialog.cancel();
                            }
                        })
                        .setNeutralButton("Modify", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            	//Toast.makeText(getApplicationContext(),"Not available now\n(Please wait v. 0.5.2)", Toast.LENGTH_LONG).show();
                                
                            	ArrayList <String> old=new ArrayList <String>();
                            	
                                try {
									JSONObject jo=new JSONObject (readJson(pathAccount));
									JSONArray ja=jo.getJSONArray("nome");
									old.add(ja.getString(posx));
									ja=jo.getJSONArray("user");
									old.add(ja.getString(posx));
									ja=jo.getJSONArray("psw");
									old.add(ja.getString(posx));
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
                            	
                            	//CANCELLO
                            	deleteItem(posx);
                                item.removeAllViews();
    							item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
    							nomeProfilo=(TextView)findViewById(R.id.profilo);
    							numAccount=(TextView)findViewById(R.id.nAccount);
    							try {
    								nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
    								numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
    							} catch (JSONException e1) {
    								// TODO Auto-generated catch block
    								e1.printStackTrace();
    								if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
    									numAccount.setText("1");	
    								}else{
    									numAccount.setText("0");
    								}
    							}
    							
    							//REINSERISCO
    							item.removeAllViews();
    							item.addView(getLayoutInflater().inflate(R.layout.add_frag, null));
    							
    							//load button
    							fragAddBtNext=(Button)findViewById(R.id.frag_add_bt_next);
    							fragAddBtNext.setText("Modify");
    							
    							EditText b1=(EditText)findViewById(R.id.frag_add_ac_name);
    							b1.setText(old.get(0));
    							EditText b2=(EditText)findViewById(R.id.frag_add_ac_user);
    							b2.setText(old.get(1));
    							EditText b3=(EditText)findViewById(R.id.frag_add_ac_pass);
    							b3.setText(old.get(2));
    							
    							//---------------frag add------------------
    					        fragAddBtNext.setOnClickListener(new View.OnClickListener(){
    					            public void onClick(View view){
    					            	String fragAddNomeAcc=(((EditText)findViewById(R.id.frag_add_ac_name)).getText()).toString();
    					            	String fragAddUser=(((EditText)findViewById(R.id.frag_add_ac_user)).getText()).toString();
    					            	String fragAddPsw=(((EditText)findViewById(R.id.frag_add_ac_pass)).getText()).toString();
    					            	
    					            	//fragAddPsw=criptaPass(fragAddPsw);
    					            	writeJsonAccount(readJson(pathAccount),fragAddNomeAcc,fragAddUser,fragAddPsw);
    					            	item.removeAllViews();
    									item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
    									nomeProfilo=(TextView)findViewById(R.id.profilo);
    									numAccount=(TextView)findViewById(R.id.nAccount);
    									try {
    										nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
    										numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
    									} catch (JSONException e1) {
    										// TODO Auto-generated catch block
    										e1.printStackTrace();
    										if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
    											numAccount.setText("1");	
    										}else{
    											numAccount.setText("0");
    										}
    									}
    					            }
    					            
    							});
    							
    							//CHIUDO LA FINESTRA DI DIALOGO
                            	dialog.cancel();
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        });
        
    }
  //---------------------------------------------------------------------------------------
  	public String criptaPass(String pass){
  		
  		try {
              String plaintext = pass;
              MessageDigest m = MessageDigest.getInstance("MD5");
              m.reset();
              m.update(plaintext.getBytes());
              byte[] digest = m.digest();
              BigInteger bigInt = new BigInteger(1,digest);
              String hashtext = bigInt.toString(16);
              // Now we need to zero pad it if you actually want the full 32 chars.
              while(hashtext.length() < 32 ){
                  hashtext = "0"+hashtext;
              }
              pass=hashtext;
          }catch(Exception e){
              e.printStackTrace();
          }
  		
  		return pass;
  	}
  	//-//-----------------------------------------------------------------------------------------
	public boolean writeJsonAccount(String json,String nome,String user,String password){
		
		String message ="";
		boolean ret=false;
		
		if(json.equalsIgnoreCase("")){
			try {
				JSONObject p= new JSONObject();
	            p.put("nome", nome);
	            p.put("user", user);
	            p.put("psw", password);
	            message=p.toString();
	        } catch (JSONException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
			
		}else{
			try {
				JSONObject p= new JSONObject(json);
	            p.accumulate("nome", nome);
	            p.accumulate("user", user);
	            p.accumulate("psw", password);
	            message=p.toString();
	        } catch (JSONException e) {
	            // TODO Auto-generated catch block
	            e.printStackTrace();
	        }
			
		}
		
        try{
        	
            FileOutputStream fos=openFileOutput(pathAccount, MODE_PRIVATE);
            OutputStreamWriter osw= new OutputStreamWriter(fos);
            try{
                osw.write(message);
                osw.flush();
                osw.close();
                //Toast.makeText(this, "Account added!", Toast.LENGTH_SHORT).show();
                ret=true;
            }catch(IOException e){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
		
		return ret;
	}
	//--------------------------------------------------------------------------------------------

    public void selectItem(int position) {
		listView.setItemChecked(position, true);
		setTitle(menu[position]);
	}
    
    public void setTitle(String title){
    	getActionBar().setTitle(title);
    }
    

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
	
	public String readJson(String path){
		
		int data_block=10000;
		String final_data="";
		
		try{
            FileInputStream fis=openFileInput(path);
            InputStreamReader isr=new InputStreamReader(fis);
            char[] data=new char[data_block];
            int size;
            try{
                while((size=isr.read(data))>0){
                    String read_data=String.copyValueOf(data,0,size);
                    final_data+=read_data;
                    data= new char [data_block];
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }catch(FileNotFoundException e){
            e.printStackTrace();
        }
		
		return final_data;
	}
	
	void verificaProfilo(String json){
		
		if(json.equalsIgnoreCase("")){
            Toast.makeText(getApplicationContext(),"Creating new profile...", Toast.LENGTH_LONG).show();
            Intent a_Profilo = new Intent(MainActivity.this, Profile.class);
            finish();
            MainActivity.this.startActivity(a_Profilo);
        }else{
        	RelativeLayout item = (RelativeLayout)findViewById(R.id.main);
        	item.removeAllViews();
        	//sostituire error_frag con home
			item.addView(getLayoutInflater().inflate(R.layout.home_frag, null));
			nomeProfilo=(TextView)findViewById(R.id.profilo);
			numAccount=(TextView)findViewById(R.id.nAccount);
			try {
				nomeProfilo.setText(new JSONObject(readJson(path)).getString("first")+" "+new JSONObject(readJson(path)).getString("last"));
				numAccount.setText((new JSONArray ((new JSONObject (readJson(pathAccount))).getString("nome"))).length()+"");
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				if(readJson(pathAccount)!=""&&readJson(pathAccount)!=null){
					numAccount.setText("1");	
				}else{
					numAccount.setText("0");
				}
			}
			Button setup=(Button)findViewById(R.id.setup);
	        setup.setOnClickListener(new View.OnClickListener(){
	            public void onClick(View view){
	            	
	            	Toast.makeText(getApplicationContext(),"Not available now\n(Please wait v. 0.5.3)", Toast.LENGTH_LONG).show();
                    	            	
	            }
	        });
        }
		
	}
	boolean verificaPasscode(String p){
		
		try {
			if(((new JSONObject (readJson(path))).getString("passcode")).equalsIgnoreCase(p)){
				return true;
			}
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		return false;
	}
	//--------------------------------------------------------------------------------------
	void deleteItem(int pos){
		
		String fileRead=readJson(pathAccount);
		JSONObject jOb;
		JSONArray jAr;
		ArrayList <String> a=new ArrayList <String>();
		ArrayList <String> b=new ArrayList <String>();
		ArrayList <String> c=new ArrayList <String>();
		
		try {
			jOb=new JSONObject (fileRead);
			jAr=jOb.getJSONArray("nome");
			for(int i=0;i<jAr.length();i++){
				a.add(jAr.getString(i));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			jOb=new JSONObject (fileRead);
			jAr=jOb.getJSONArray("user");
			for(int i=0;i<jAr.length();i++){
				b.add(jAr.getString(i));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			jOb=new JSONObject (fileRead);
			jAr=jOb.getJSONArray("psw");
			for(int i=0;i<jAr.length();i++){
				c.add(jAr.getString(i));
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		a.remove(pos);
		b.remove(pos);
		c.remove(pos);
		
		String g="";
		for(int i=0;i<a.size();i++){
			writeJsonAccount(g,a.get(i),b.get(i),c.get(i));
			g=readJson(pathAccount);
		}
		Toast.makeText(getApplicationContext(),"All done!", Toast.LENGTH_LONG).show();
	}
	
	class ListViewItem
    {
        public String Title;
        public String SubTitle;
    }
	
}
