package com.mzlabs.passwordkeeperoffline;

import java.util.List;
import com.mzlabs.passwordkeeperoffline.MainActivity.ListViewItem;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class CustomListViewAdapter extends BaseAdapter{
	
	LayoutInflater inflater;
    List<ListViewItem> items;

    public CustomListViewAdapter(Activity context, List<ListViewItem> items) {
        super();

        this.items = items;
        this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return items.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ListViewItem item=items.get(position);

        View vi=convertView;

        if(convertView==null)
            vi = inflater.inflate(R.layout.da_item, null);

        TextView TxtTitle= (TextView) vi.findViewById(R.id.txtTitle);
        TextView TxtSubTitle= (TextView) vi.findViewById(R.id.txtSubTitle);

        TxtTitle.setText(item.Title);
        TxtSubTitle.setText(item.SubTitle);

        return vi;
    }
}
