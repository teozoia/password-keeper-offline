package com.mzlabs.passwordkeeperoffline;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class ReadFile extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_read_file);
		
		
		String title = getIntent().getStringExtra("title");
		String path = getIntent().getStringExtra("path");
		String json = getIntent().getStringExtra("json");
		
		TextView titlev =(TextView)findViewById(R.id.tit);
		titlev.setText(title);
		TextView pathv =(TextView)findViewById(R.id.path);
		pathv.setText(path);
		TextView jsonv =(TextView)findViewById(R.id.json);
		try {
			jsonv.setText((new JSONObject (json)).toString(4));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			if(json.equalsIgnoreCase("")){
				jsonv.setText("The file is busy, you can't read it now!");
			}else{
				jsonv.setText(json);
			}
			
		}
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.read_file, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
